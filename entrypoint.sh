#!/bin/bash

service cups start
service dbus start

xpra start-desktop \
	--pulseaudio=no \
	--mdns=no \
	--mmap=yes \
	--bind-tcp=0.0.0.0:8080 \
	--html=on \
	--no-daemon \
	--exit-with-children \
	--dpi=100 \
	--start-child="mate-session" \
	--xvfb='/usr/bin/Xorg -dpi 100 -noreset -nolisten tcp +extension GLX +extension RANDR +extension RENDER -logfile /tmp/Xorg.log -config /etc/xpra/xorg.conf'