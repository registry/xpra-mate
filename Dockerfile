FROM registry.gitlab.com/registry/xpra-base:master

RUN echo "force-unsafe-io" > /etc/dpkg/dpkg.cfg.d/02apt-speedup \
	&& echo "Acquire::http {No-Cache=True;};" > /etc/apt/apt.conf.d/no-cache \
	&& apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    	mate-core \
    	language-pack-gnome-ru language-selector-common hyphen-ru mythes-ru myspell-ru \
    	python-software-properties software-properties-common \
	&& apt-add-repository multiverse \
	&& apt-add-repository universe \
	&& echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections \
    && apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    	ttf-mscorefonts-installer \
    && apt-get autoremove -y && apt-get clean && rm -rf /var/lib/apt/lists/* /var/tmp/*

ADD ./entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
